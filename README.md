1. clone the repository

2. create folder app/code/Mbs/OrderNotifification when located at the root of the Magento site

3. copy the content of this repository within the folder

4. install the module php bin/magento setup:upgrade

5. place an order. Make sure there is an invoice against the order. Finally, ship the order, you should see a new comment at the bottom of the order view in the backend

