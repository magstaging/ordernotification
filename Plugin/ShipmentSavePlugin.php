<?php
/**
 * Created by PhpStorm.
 * User: hervetribouilloy
 * Date: 15/09/2018
 * Time: 19:39
 */

namespace Mbs\OrderNotifification\Plugin;


use Magento\Sales\Api\OrderRepositoryInterface;

class ShipmentSavePlugin
{
    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    public function __construct(
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository
    ) {
        $this->orderRepository = $orderRepository;
    }

    public function afterRegister(\Magento\Sales\Model\Order\Shipment $shipment)
    {
        try {
            $order = $this->orderRepository->get($shipment->getOrderId());
            $order->addStatusHistoryComment('Order Complete', \Magento\Sales\Model\Order::STATE_COMPLETE);
        } catch (\Exception $e) {

        }
    }
}